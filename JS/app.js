function itemPet() {
  let numberPet = document.getElementById("pet");
  let itemPet = document.getElementById("table_pet");
  for (let i = 0; i <= numberPet.value - 1; i++) {
    let item = document.createElement("div");
    let itemPetName = document.createElement("input");
    let itemPetDot = document.createElement("input");
    itemPetDot.setAttribute("type", "radio");
    itemPetDot.setAttribute("name", `itemPet${i}`);
    let itemPetCat = document.createElement("input");
    itemPetCat.setAttribute("type", "radio");
    itemPetCat.setAttribute("name", `itemPet${i}`);
    item.appendChild(itemPetName);
    item.appendChild(itemPetDot);
    item.appendChild(itemPetCat);
    itemPet.appendChild(item);
  }
  let tablePetSubmit = document.createElement("button");
  tablePetSubmit.setAttribute("type", "submit");
  tablePetSubmit.innerHTML = "Guardar";
  tablePetSubmit.addEventListener("click", formPett);
  itemPet.appendChild(tablePetSubmit);
}

let registros = [];

function formPett() {
  let form = document.forms["table_pet"];
  const registro = {
      name: form.elements[0].value,
      dot: form.elements[1].checked,
      cat: form.elements[2].checked,
    };
    registros.push(registro);
    form.reset();
    console.log(registros);
}

